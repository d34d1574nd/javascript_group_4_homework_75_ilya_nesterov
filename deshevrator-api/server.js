const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

app.post('/encode', (req, res) => {
    console.log(req.body);
    const encode = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encode});
});

app.post('/decode', (req, res) => {
    const decode = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({decode});
});

app.listen(port, () => {
    console.log('Server on ' + port)
});