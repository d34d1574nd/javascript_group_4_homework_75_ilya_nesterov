import React, {Component} from 'react';

import './DeshevratorForm.css';
import {changeValue, textDecode, textEncoded} from "../../store/action";
import {connect} from "react-redux";

class DeshevratorForm extends Component {
    submitFormHandlerDecode = event => {
        event.preventDefault();
        const message = {
            message: this.props.state.decode,
            password: this.props.state.password
         };
         this.props.decodeMessage(message)
    };

    submitFormHandlerEncode = event => {
        event.preventDefault();
        const message = {
            message: this.props.state.encode,
            password: this.props.state.password
        };
        this.props.encodeMessage(message)
    };

    render () {
        return (
            <form style={{width: '400px', margin: '0 auto'}}>
                <label htmlFor="decode">Decoded message:</label>
                <textarea type="text" name={"encode"} placeholder='Enter decode message' onChange={this.props.changeValue} value={this.props.state.encode}/>
                <input type="text"  name={"password"}  placeholder="Enter password" value={this.props.state.password} onChange={this.props.changeValue}/>
                <button type="submit" onClick={event => this.submitFormHandlerEncode(event)}><i className="fas fa-arrow-down" /></button>
                <button type="submit" onClick={event => this.submitFormHandlerDecode(event)}><i className="fas fa-arrow-up" /></button>
                <label htmlFor="encode">Encoded message:</label>
                <textarea type="text" name={"decode"} placeholder="Enter encode message" onChange={this.props.changeValue}  value={this.props.state.decode}/>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    state: state,
});

const mapDispatchToProps = dispatch => ({
    changeValue: event => dispatch(changeValue(event)),
    encodeMessage: message => dispatch(textEncoded(message)),
    decodeMessage: message => dispatch(textDecode(message)),
});


export default connect(mapStateToProps, mapDispatchToProps)(DeshevratorForm);