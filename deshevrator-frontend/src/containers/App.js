import React, { Component } from 'react';

import './App.css';
import DeshevratorForm from "../components/DeshevratorForm/DeshevratorForm";

class App extends Component {
  render() {
    return (
      <div className="App">
        <DeshevratorForm/>
      </div>
    );
  }
}

export default App;
