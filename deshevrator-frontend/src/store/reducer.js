
import {FETCH_TEXT_DECODED, FETCH_TEXT_ENCODED, VALUE} from "./action";

const initialState = {
    decode: '',
    encode: '',
    password: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case VALUE:
            return {...state, [action.event.target.name]: action.event.target.value};
        case FETCH_TEXT_ENCODED:
            return {...state, decode: action.message};
        case FETCH_TEXT_DECODED:
            return {...state, encode: action.message};
        default:
            return state;
    }
};

export default reducer;