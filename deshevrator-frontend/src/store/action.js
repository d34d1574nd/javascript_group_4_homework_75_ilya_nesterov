import axios from "../axios-deshevrator";

export const VALUE = 'VALUE';
export const FETCH_TEXT_ENCODED = 'FETCH_TEXT_ENCODED';
export const FETCH_TEXT_DECODED = 'FETCH_TEXT_DECODED';

export const changeValue = event => ({type: VALUE, event});
export const fetchTextEncoded = message => ({type: FETCH_TEXT_ENCODED, message});
export const fetchTextDecode = message => ({type: FETCH_TEXT_DECODED, message});

export const textDecode = message => {
    return dispatch => {
        return axios.post('/decode', message).then(
            response => dispatch(fetchTextDecode(response.data.decode))
        )
    }
};

export const textEncoded = message => {
    return dispatch => {
        return axios.post('/encode', message).then(
            response => dispatch(fetchTextEncoded(response.data.encode))
        )
    }
};

